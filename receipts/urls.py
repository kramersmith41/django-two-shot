from django.urls import path
from receipts.views import (
    account_list,
    categery_list,
    create_account,
    create_category,
    list_receipts,
    create_receipt,
)

urlpatterns = [
    path("", list_receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", categery_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
